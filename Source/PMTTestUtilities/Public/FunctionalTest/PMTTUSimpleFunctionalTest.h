// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "FunctionalTest.h"
#include "PMTTUSimpleFunctionalTest.generated.h"


/**
 * Basic functional test that provides functionality
 * to create timers and actors.
 */
UCLASS()
class PMTTESTUTILITIES_API APMTTUSimpleFunctionalTest : public AFunctionalTest
{
	GENERATED_BODY()

public:

	/** Spawn an actor with a given tag so we can get it if needed. */
	template<typename T>
	T* CreateTestActor(TSubclassOf<T> InActorClass, const FName& ActorTag = FName());
	/** Get a created actor by tag. */
	template<typename T>
	T* GetCreatedActor(const FName& ActorTag);

	/** Create a timer for this test. */
	FTimerHandle CreateTimerForTest(float TimeSeconds, FTimerDelegate TimerDel, bool bLoop = false);
	/** Bind delegate to clear all created actors and timers. */
	void PrepareTest() override;

protected:

	/** Set test name to SimpleTestName if overridden. */
	void OnConstruction(const FTransform& Transform) override;

	/** Clear all created actors and timers. */
	UFUNCTION()
	virtual void HandleTestFinished();

	/** Name of the test (if empty it does not override anything). */
	FString SimpleTestName;

private:

	/** All actors created for this test. */
	UPROPERTY(Transient)
	TArray<AActor*> CreatedActors;
	/** All timers created for this test. */
	UPROPERTY(Transient)
	TArray<FTimerHandle> TimerHandles;
	
};

template<typename T>
T* APMTTUSimpleFunctionalTest::CreateTestActor(TSubclassOf<T> InActorClass, const FName& ActorTag)
{
	T* SpawnedActor = GetWorld()->SpawnActor<T>(InActorClass);
	if (!ActorTag.IsNone())
	{
		SpawnedActor->Tags.Add(ActorTag);
	}
	CreatedActors.Add(SpawnedActor);
	return SpawnedActor;
}

template<typename T>
T* APMTTUSimpleFunctionalTest::GetCreatedActor(const FName& ActorTag)
{
	for (AActor* SingleActor : CreatedActors)
	{
		if (SingleActor && !SingleActor->IsActorBeingDestroyed() && SingleActor->Tags.Contains(ActorTag))
		{
			return Cast<T>(SingleActor);
		}
	}
	return nullptr;
}
