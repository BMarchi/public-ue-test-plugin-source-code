// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "FunctionalTest.h"
#include "TimerManager.h"

#include "PMTTUNetworkFunctionalTest.generated.h"

class AController;
class APMTTUBaseGameMode;
class APMTTUNetworkProxy;

DECLARE_LOG_CATEGORY_EXTERN(APMTTUNetworkFunctionalTestLog, Log, All);

/**
 * Base class for tests that involve networking.
 */
UCLASS()
class PMTTESTUTILITIES_API APMTTUNetworkFunctionalTest : public AFunctionalTest
{
	GENERATED_BODY()

protected:

	/** Create an unique proxy for given clients. */
	void CreateProxiesForClients(TArray<AController*> ClientControllers);
	/** Destroy all proxies from clients. */
	void KillClientProxies();
	/** Waits a given time and calls the given delegate once it's fulfilled. */
	void WaitDelay(float SecondsToWait, FTimerDelegate Delegate);
	/** Verify if clients didn't join in time. */
	void Tick(float DeltaSeconds) override;
	/** Create proxies for clients. */
	void StartTest() override;
	/** Connect required delegates. */
	void PrepareTest() override;
	/** Verify that all clients connected. */
	bool IsReady_Implementation() override;
	/** Client logged in. */
	UFUNCTION()
	virtual void OnClientLoggedIn(APMTTUBaseGameMode* GameMode, AController* InController);
	/** Unbind all delegates and leave world in the same state it was before running the test. */
	UFUNCTION()
	virtual void HandleTestFinished();
	/** Callback called when a proxy finished testing. */
	virtual void OnProxyFinishedTest(AActor* InProxy, EFunctionalTestResult InResult, const FString& InMessage);

	/** Network proxies for each client. */
	UPROPERTY(Transient)
	TMap<AController*, APMTTUNetworkProxy*> ClientProxies;
	/** Result used when the clients were not able to join in time. */
	UPROPERTY(EditAnywhere, Category = "Client Timeout")
	EFunctionalTestResult ClientTimeoutResult;
	/** Required clients to start the test. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 AmountOfClients = 2;
	/** How many seconds we wait for clients to connect. Test will fail if the time is reached. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float TimeForClientsToJoin = 3.0f;
	/** Track time for clients to join. */
	float CurrentTimeForClientsToJoin = 0.0f;
	/** Track the amount of clients that connected. */
	int32 CurrentClients = 0;
	/** Amount of clients that finished testing. */
	int32 ClientsThatFinishedTesting = 0;
	/** Amount of clients that finished testing successfully. */
	int32 ClientsThatFinishedTestingSuccessfully = 0;
	/** Determines if all clients connected so we stop counting the timer. */
	bool bAllClientsConnected = false;
	/** Handles of timers. Will be cleared when test ends. */
	TArray<FTimerHandle> TimerHandles;
	
};
