// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "PMTTUBaseGameMode.generated.h"

class AController;
class APMTTUBaseGameMode;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FPMTUTOnClientLoggedInDelegate, APMTTUBaseGameMode*, GameMode, AController*, PlayerController);

/**
 * Base game mode that provides a set of utilities for automation tests.
 */
UCLASS()
class PMTTESTUTILITIES_API APMTTUBaseGameMode : public AGameMode
{
	GENERATED_BODY()

public:

	/** Delegate fired when a client logs in. */
	UPROPERTY(BlueprintAssignable)
	FPMTUTOnClientLoggedInDelegate ClientLoggedInDelegate;

protected:

	/** Trigger ClientLoggedInDelegate delegate */
	void GenericPlayerInitialization(AController* C) override;
	
};
