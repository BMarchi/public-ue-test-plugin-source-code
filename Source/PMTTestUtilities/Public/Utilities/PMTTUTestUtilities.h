// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "FunctionalTest.h"

#include "Kismet/BlueprintFunctionLibrary.h"

#include "PMTTUTestUtilities.generated.h"

/** Helper macro to check for a condition and finish the test if the condition is false. */
#define PMTTU_FAIL_AND_RETURN_IF_FALSE(FunctionalTestFun, InMessage) \
if (!FunctionalTestFun) \
{ \
	FinishTest(EFunctionalTestResult::Failed, InMessage); \
	return; \
}

/** Helper macro to finish the test if the result succeeded. */
#define PMTTU_SUCCESS_AND_RETURN_IF_TRUE(FunctionalTestFun, InMessage) \
if (FunctionalTestFun) \
{ \
	FinishTest(EFunctionalTestResult::Succeeded, InMessage); \
	return; \
}

/** Helper macro to check for a condition and finish the test. */
#define PMTTU_CHECK_TEST_COND_AND_RETURN(FunctionalTestFun, InMessage) \
if (FunctionalTestFun) \
{ \
	FinishTest(EFunctionalTestResult::Succeeded, InMessage); \
	return; \
} \
else \
{ \
	FinishTest(EFunctionalTestResult::Failed, InMessage); \
	return; \
}

/** Utility to declare the test for our simple test. This can go either in our cpp or h file. */
#define PMTTU_DEFINE_SIMPLE_FUNCTIONAL_TEST(ClassName, InTestName, InDescription) \
ClassName::ClassName() \
{ \
SimpleTestName = InTestName; \
Description = InDescription; \
} \
void ClassName::StartTest()

/** Call parent implementation of the simple functional test. */
#define PMTTU_SIMPLE_FUNCTIONAL_TEST_PARENT() \
Super::StartTest();

/** An example for the usage of these macros is the following:
* UCLASS()
* class AExampleSimpleFunctionalTest : public APMTTUSimpleFunctionalTest
* {
*    PMTTU_DECLARE_SIMPLE_FUNCTIONAL_TEST(AExampleSimpleFunctionalTest, "ExampleTest", "This is just a basic sample for the macro usage.");
* };
*
* PMTTU_DEFINE_SIMPLE_FUNCTIONAL_TEST(AExampleSimpleFunctionalTest)
* {
*   PMTTU_SIMPLE_FUNCTIONAL_TEST_PARENT();
*   // Do our stuff
* }
*
**/

DECLARE_DELEGATE_ThreeParams(FPMTTUFinishTestDelegate, AActor*, EFunctionalTestResult, const FString&);

class APMTTUNetworkProxy;

UCLASS()
class PMTTESTUTILITIES_API UPMTTUTestUtilitiesFunction : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	/** Retrieve the network proxy that corresponds to the given controller. */
	UFUNCTION(BlueprintPure)
	static APMTTUNetworkProxy* GetNetworkProxyFromController(AController* InController);
	/** Retrieve the first client controller we find. */
	UFUNCTION(BlueprintPure)
	static AController* GetFirstClientController(const UObject* WorldContext);
};

