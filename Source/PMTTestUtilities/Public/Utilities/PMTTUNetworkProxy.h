// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "FunctionalTest.h"

#include "Utilities/PMTTUTestUtilities.h"

#include "PMTTUNetworkProxy.generated.h"

class AController;

/** Basic proxy that has the purpose of doing rpcs in tests. */
UCLASS()
class PMTTESTUTILITIES_API APMTTUNetworkProxy : public AActor
{
	GENERATED_BODY()
	
public:

	APMTTUNetworkProxy();

	UFUNCTION(Server, Reliable)
	void ServerFinishTesting(EFunctionalTestResult InResult, const FString& InMessage);

	virtual void ServerFinishTesting_Implementation(EFunctionalTestResult InResult, const FString& InMessage);
	virtual bool ServerFinishTesting_Validate(EFunctionalTestResult InResult, const FString& InMessage);

	void SetNetworkOwner(AController* InOwner);

	/** Delegate fired when a client finished the test. */
	FPMTTUFinishTestDelegate FinishTestDelegate;

protected:

	/** Owner of this proxy. Only valid in server. */
	UPROPERTY(Transient)
	AController* OwnerPC = nullptr;

};
