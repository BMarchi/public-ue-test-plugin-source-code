// Copyright © Brian Ezequiel Marchi. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DefaultPawn.h"

#include "AbilitySystemComponent.h"
#include "AbilitySystemInterface.h"

#include "PMTGUDefaultPawnGASTest.generated.h"

/**
 * Default pawn that contains an ability system component to test
 * anything related with GAS world.
 */
UCLASS()
class PMTTESTUTILITIES_API APMTGUDefaultPawnGASTest : public ADefaultPawn, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:

	/** Create ASC. */
	APMTGUDefaultPawnGASTest();
	/** Initialize ASC. */
	void PostInitializeComponents() override;
	/** Get the underlying ability system component. */
	UAbilitySystemComponent* GetAbilitySystemComponent() const override;

protected:

	/** Make sure to initialize the ability system component. */
	void BeginPlay() override;

private:

	/** ASC used for testing. */
	UPROPERTY(Category = AbilitySystem, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UAbilitySystemComponent* AbilitySystemComponent;
	
};
