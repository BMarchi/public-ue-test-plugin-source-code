// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "FunctionalTest/PMTTUSimpleFunctionalTest.h"

// UE Includes
#include "Components/TextRenderComponent.h"
#include "TimerManager.h"

FTimerHandle APMTTUSimpleFunctionalTest::CreateTimerForTest(float TimeSeconds, FTimerDelegate TimerDel, bool bLoop)
{
	FTimerHandle SingleHandle;
	GetWorld()->GetTimerManager().SetTimer(SingleHandle, TimerDel, TimeSeconds, bLoop);
	return TimerHandles.Add_GetRef(SingleHandle);
}

void APMTTUSimpleFunctionalTest::PrepareTest()
{
	Super::PrepareTest();

	OnTestFinished.AddDynamic(this, &APMTTUSimpleFunctionalTest::HandleTestFinished);
}

void APMTTUSimpleFunctionalTest::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	if (TestName && !SimpleTestName.IsEmpty())
	{
		if (bIsEnabled)
		{
			TestName->SetText(FText::FromString(SimpleTestName));
		}
		else
		{
			TestName->SetText(FText::FromString(SimpleTestName + TEXT("\n") + TEXT("# Disabled #")));
		}
	}
}

void APMTTUSimpleFunctionalTest::HandleTestFinished()
{
	OnTestFinished.RemoveDynamic(this, &APMTTUSimpleFunctionalTest::HandleTestFinished);

	for (AActor* SingleActor : CreatedActors)
	{
		if (SingleActor && !SingleActor->IsActorBeingDestroyed())
		{
			SingleActor->Destroy();
		}
	}
	CreatedActors.Empty();

	for (FTimerHandle& TimerHandle : TimerHandles)
	{
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle);
	}
	TimerHandles.Empty();
}
