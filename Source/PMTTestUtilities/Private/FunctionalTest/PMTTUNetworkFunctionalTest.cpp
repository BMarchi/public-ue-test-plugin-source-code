// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "FunctionalTest/PMTTUNetworkFunctionalTest.h"

// PMTTU Includes
#include "GameModes/PMTTUBaseGameMode.h"
#include "Utilities/PMTTUNetworkProxy.h"

// UE Includes
#include "GameFramework/PlayerController.h"

DEFINE_LOG_CATEGORY(APMTTUNetworkFunctionalTestLog);

void APMTTUNetworkFunctionalTest::CreateProxiesForClients(TArray<AController*> ClientControllers)
{
	KillClientProxies();
	for (AController* SingleController : ClientControllers)
	{
		APMTTUNetworkProxy* ClientProxy = GetWorld()->SpawnActor<APMTTUNetworkProxy>();
		ClientProxy->SetNetworkOwner(SingleController);
		ClientProxy->FinishTestDelegate.BindUObject(this, &APMTTUNetworkFunctionalTest::OnProxyFinishedTest);
		ClientProxies.Add(SingleController, ClientProxy);
	}
}

void APMTTUNetworkFunctionalTest::KillClientProxies()
{
	for (auto& ProxyPair : ClientProxies)
	{
		APMTTUNetworkProxy* ClientProxy = ProxyPair.Value;
		if (ClientProxy && ClientProxy->IsValidLowLevel())
		{
			ClientProxy->FinishTestDelegate.Unbind();
			ClientProxy->Destroy();
		}
	}
	ClientProxies.Empty();
}

void APMTTUNetworkFunctionalTest::WaitDelay(float SecondsToWait, FTimerDelegate Delegate)
{
	FTimerHandle Handle;
	GetWorld()->GetTimerManager().SetTimer(Handle, Delegate, SecondsToWait, false);
	TimerHandles.Add(Handle);
}

void APMTTUNetworkFunctionalTest::Tick(float DeltaSeconds)
{
	if (IsRunning() && !bAllClientsConnected)
	{
		CurrentTimeForClientsToJoin += DeltaSeconds;
		if (CurrentTimeForClientsToJoin > TimeForClientsToJoin)
		{
			FinishTest(ClientTimeoutResult, FString(TEXT("Clients timed out in Test ") + GetName()));
		}
	}
	Super::Tick(DeltaSeconds);
}

void APMTTUNetworkFunctionalTest::StartTest()
{
	Super::StartTest();
	TArray<AController*> PlayerControllers;
	for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		APlayerController* PlayerController = Iterator->Get();
		if (PlayerController && !PlayerController->IsLocalController())
		{
			PlayerControllers.Add(PlayerController);
		}
	}
	CreateProxiesForClients(PlayerControllers);
}

void APMTTUNetworkFunctionalTest::PrepareTest()
{
	APMTTUBaseGameMode* GameMode = CastChecked<APMTTUBaseGameMode>(GetWorld()->GetAuthGameMode());
	if (GameMode)
	{
		GameMode->ClientLoggedInDelegate.AddDynamic(this, &APMTTUNetworkFunctionalTest::OnClientLoggedIn);
		// In case we were running a test and clients are already logged in.
		CurrentClients = GameMode->GetNumPlayers();
		bAllClientsConnected = CurrentClients == AmountOfClients;
	}
	OnTestFinished.AddDynamic(this, &APMTTUNetworkFunctionalTest::HandleTestFinished);
}

bool APMTTUNetworkFunctionalTest::IsReady_Implementation()
{
	return Super::IsReady_Implementation() && CurrentClients == AmountOfClients;
}

void APMTTUNetworkFunctionalTest::OnClientLoggedIn(APMTTUBaseGameMode* GameMode, AController* InController)
{
	++CurrentClients;
	bAllClientsConnected = CurrentClients == AmountOfClients;
}

void APMTTUNetworkFunctionalTest::HandleTestFinished()
{
	APMTTUBaseGameMode* GameMode = CastChecked<APMTTUBaseGameMode>(GetWorld()->GetAuthGameMode());
	if (GameMode)
	{
		GameMode->ClientLoggedInDelegate.RemoveDynamic(this, &APMTTUNetworkFunctionalTest::OnClientLoggedIn);
	}
	OnTestFinished.RemoveDynamic(this, &APMTTUNetworkFunctionalTest::HandleTestFinished);
	for (FTimerHandle SingleHandle : TimerHandles)
	{
		GetWorld()->GetTimerManager().ClearTimer(SingleHandle);
	}
	TimerHandles.Empty();
	KillClientProxies();
}

void APMTTUNetworkFunctionalTest::OnProxyFinishedTest(AActor* InProxy, EFunctionalTestResult InResult, const FString& InMessage)
{
	++ClientsThatFinishedTesting;
	if (InResult == EFunctionalTestResult::Succeeded)
	{
		++ClientsThatFinishedTestingSuccessfully;
	}
	else if (InResult == EFunctionalTestResult::Failed)
	{
		UE_LOG(APMTTUNetworkFunctionalTestLog, Error, TEXT("Client with Proxy '%s' failed the test with message: '%s'"), InProxy ? *InProxy->GetName() : TEXT("NoProxy"), *InMessage);
	}
	if (ClientsThatFinishedTesting == AmountOfClients)
	{
		if (ClientsThatFinishedTestingSuccessfully == AmountOfClients)
		{
			FinishTest(EFunctionalTestResult::Succeeded, FString());
		}
		else
		{
			FinishTest(EFunctionalTestResult::Failed, InMessage);
		}
	}
}
