// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "Utilities/PMTTUTestUtilities.h"

// PMTTU Includes
#include "Utilities/PMTTUNetworkProxy.h"

APMTTUNetworkProxy* UPMTTUTestUtilitiesFunction::GetNetworkProxyFromController(AController* InController)
{
	if (!InController)
	{
		return nullptr;
	}

	for (AActor* Child : InController->Children)
	{
		APMTTUNetworkProxy* Proxy = Cast<APMTTUNetworkProxy>(Child);
		if (Proxy)
		{
			return Proxy;
		}
	}
	return nullptr;
}

AController* UPMTTUTestUtilitiesFunction::GetFirstClientController(const UObject* WorldContext)
{
	if (!WorldContext)
	{
		return nullptr;
	}
	for (FConstPlayerControllerIterator Iterator = WorldContext->GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		APlayerController* PlayerController = Iterator->Get();
		if (PlayerController && !PlayerController->IsLocalController())
		{
			return PlayerController;
		}
	}
	return nullptr;
}
