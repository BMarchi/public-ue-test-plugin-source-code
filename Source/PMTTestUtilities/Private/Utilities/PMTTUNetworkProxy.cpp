// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "Utilities/PMTTUNetworkProxy.h"

APMTTUNetworkProxy::APMTTUNetworkProxy()
{
	PrimaryActorTick.bCanEverTick = false;
	SetReplicates(true);
	bAlwaysRelevant = true;
}

void APMTTUNetworkProxy::ServerFinishTesting_Implementation(EFunctionalTestResult InResult, const FString& InMessage)
{
	FinishTestDelegate.Execute(this, InResult, InMessage);
}

bool APMTTUNetworkProxy::ServerFinishTesting_Validate(EFunctionalTestResult InResult, const FString& InMessage)
{
	return true;
}

void APMTTUNetworkProxy::SetNetworkOwner(AController* InOwner)
{
	SetOwner(InOwner);
	OwnerPC = InOwner;
}
