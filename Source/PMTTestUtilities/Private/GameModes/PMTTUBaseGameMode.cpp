// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "GameModes/PMTTUBaseGameMode.h"

void APMTTUBaseGameMode::GenericPlayerInitialization(AController* C)
{
	Super::GenericPlayerInitialization(C);
	ClientLoggedInDelegate.Broadcast(this, C);
}
