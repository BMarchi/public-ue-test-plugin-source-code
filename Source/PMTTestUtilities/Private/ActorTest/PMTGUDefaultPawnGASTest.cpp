// Copyright © Brian Ezequiel Marchi. All rights reserved.


#include "ActorTest/PMTGUDefaultPawnGASTest.h"

APMTGUDefaultPawnGASTest::APMTGUDefaultPawnGASTest()
{
	AbilitySystemComponent = CreateDefaultSubobject<UAbilitySystemComponent>(TEXT("AbilitySystemComponent"));
	AbilitySystemComponent->SetIsReplicated(true);
}

void APMTGUDefaultPawnGASTest::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	AbilitySystemComponent->InitAbilityActorInfo(this, this);
}

UAbilitySystemComponent* APMTGUDefaultPawnGASTest::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

void APMTGUDefaultPawnGASTest::BeginPlay()
{
	Super::BeginPlay();
	AbilitySystemComponent->InitAbilityActorInfo(this, this);
}
